import { Component } from '@angular/core';
import { ListModuleDataService } from './list.service';

@Component({
    selector   : 'app-jobs',
    templateUrl: './list.component.html',
    styleUrls  : ['./list.component.scss']
})

export class ListComponent {

  constructor(public mDataService: ListModuleDataService) {
    this.mDataService.loadRecs();
    setInterval( () => this.mDataService.loadRecs(), 5000);
  }

  vehiclePlateNumber = '';
  currentVehicleView = '';

  registerNew(a) {
    if ( this.vehiclePlateNumber !== '') {
        this.mDataService.newRec(this.vehiclePlateNumber);
        this.vehiclePlateNumber = '';
    }
  }

  viewAll(id: number, vehiclePlateNumber: string) {
    this.mDataService.AllFromOne(id);
    this.currentVehicleView = vehiclePlateNumber;
  }
}
