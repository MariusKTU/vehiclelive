import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { Vehicle } from './list.model';
import { VehicleDetail } from './list.model';

@Injectable()
export class ListModuleDataService {

  recs: BehaviorSubject<Vehicle[]> = new BehaviorSubject( [] );
  allFromOneRecs: BehaviorSubject<VehicleDetail[]> = new BehaviorSubject( [] );

  constructor(private apiService: ApiService) { }


  // loads recs by specified criteria and for specified page
  public loadRecs(): void {
    this.apiService.getData( 'Manager' )
    .then( (res: Vehicle[]) => {
      this.recs.next( res );
    })
    .catch();
  }

  // Registers new vehicle to the system
  public newRec( bodyParam: string): void {
    this.apiService.insertData( 'Manager', { vehiclePlateNumber: bodyParam } )
    .then()
    .catch();
  }

  public AllFromOne(id: number) {
    this.apiService.getData( 'Manager/' + id)
    .then( (res: VehicleDetail[]) => {
      this.allFromOneRecs.next( res );
    })
    .catch();
  }
}
