export class Vehicle {
  id = 0;
  latestLatitude = 0;
  latestSpeed = 0;
  latestLongitute = 0;
  vehiclePlateNumber = '';
}

export class VehicleDetail {
  id = 0;
  latitude = 0;
  speed = 0;
  longitute = 0;
  dateAdded = '';
  vehicleId = 0;
}
