import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ListModuleDataService } from './list.service';
import { HttpClientModule } from '@angular/common/http';
// this component modules
import { ListComponent } from './list.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

const routes = [
  {
    path     : '**',
    component: ListComponent
  }
];

@NgModule({
  declarations: [
    ListComponent
  ],
  imports     : [
    RouterModule.forChild(routes),
    HttpClientModule,
    BrowserModule,
    FormsModule
  ],
  providers: [
    ListModuleDataService
  ],
})

export class ListModule {
}
