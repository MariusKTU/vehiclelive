import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ListModule } from './list/list.module';
import { ApiService } from './api/api.service';


const appRoutes: Routes = [

  {
    path     : '',
    loadChildren: './list/list.module#ListModule'
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ListModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent],
  exports     : [
    RouterModule
  ],
})

export class AppModule { }
