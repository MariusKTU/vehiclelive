import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {

  private apiBaseUrl = 'http://localhost:60348/api/';

  constructor(
    private http: HttpClient
  ) {}

  insertData(targetApi: string, paramsBody: any) {
    return this.http.post(
        this.apiBaseUrl + targetApi, paramsBody ).toPromise();
  }

  getData(targetApi: string) {
    return this.http.get(this.apiBaseUrl + targetApi).toPromise();
  }
}
