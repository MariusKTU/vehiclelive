﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class DeviceController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DeviceController(ApplicationDbContext context)
        {
            _context = context;
        }
          
        // Insert new data coming from device that is in the vehicle.
        [HttpPost]
        public object InsertFromDevice([FromBody] JObject dataFromDevice)
        {
            if (dataFromDevice == null)
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            bool hasVehicleId = dataFromDevice.ContainsKey("vehiclePlateNumber");
            bool hasLatitude = dataFromDevice.ContainsKey("latitude");
            bool haslongitude = dataFromDevice.ContainsKey("longitude");
            bool hasSpeed = dataFromDevice.ContainsKey("speed");
            // Checking if data structure is correct.
            if (!hasVehicleId || !hasLatitude || !haslongitude || !hasSpeed)
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            string vehiclePlateNumber;
            double latitude;
            double longitude;
            double speed;
            try
            {
                vehiclePlateNumber = dataFromDevice.Value<string>("vehiclePlateNumber");
                latitude = dataFromDevice.Value<double>("latitude");
                longitude = dataFromDevice.Value<double>("longitude");
                speed = dataFromDevice.Value<double>("speed");
            }
            catch
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            // Getting vehicle registration id (0 if not registered).
            int vehicleRegisterID = getVehicleId(vehiclePlateNumber);
            if (vehicleRegisterID == 0)
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            // Add record to the data list
            VehiclesData vehicle = new VehiclesData();
            string currentDate = DateTime.Now.ToString("yyyy:MM:dd h:mm:ss");
            vehicle.vehicleId = vehicleRegisterID;
            vehicle.latitude = latitude;
            vehicle.longitude = longitude;
            vehicle.speed = speed;
            vehicle.dateAdded = currentDate;
            _context.vehiclesData.Add(vehicle);
            // Update newest record.
            Vehicle latest;
            try
            {
                latest = _context.vehicles.First(row => row.id == vehicleRegisterID);
            }
            catch
            {
                // Unkown error.
                return new StatusCodeResult(500);
            }
            latest.latestLatitude = latitude;
            latest.latestLongitute = longitude;
            latest.latestSpeed = speed;
            try
            {
                _context.SaveChanges();
            }
            catch
            {
                // Unkown error.
                return new StatusCodeResult(500);
            }
            return new StatusCodeResult(204);
        }

        // Getting vehicle registration id (0 if not registered).
        private int getVehicleId(string vehiclePlateNumber)
        {
            if (vehiclePlateNumber != null)
            {
                int registeredId;
                try
                {
                    registeredId = _context.vehicles
                        .Where( condi => condi.vehiclePlateNumber == vehiclePlateNumber )
                        .Select( row => row.id )
                        .ToList()
                        .First();
                }
                catch
                {
                    return 0;
                }
                if ( registeredId > 0 )
                {
                    return registeredId;
                }
            }
            return 0;
        }

    }
}
