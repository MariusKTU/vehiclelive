﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api/Manager")]
    public class ManagerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ManagerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // Get newest data from each vehicle.
        [HttpGet]
        public object GetNewestData()
        {
            object datafromEach;
            try
            {
                datafromEach = _context.vehicles.ToList();
            }
            catch
            {
                // Unknown error.
                return new StatusCodeResult(500);
            }
            return datafromEach;
        }

        // Get all records from one vehicle.
        [HttpGet("{vehicleId}")]
        public object GetAllfromOne(int vehicleId)
        {
            object dataAll;
            try
            {
                dataAll = _context.vehiclesData.Where(row => row.vehicleId == vehicleId).ToList();
            }
            catch
            {
                // Unknown error.
                return new StatusCodeResult(500);
            }
            return dataAll;
        }

        // Register new vehicle to the system
        [HttpPost]
        public object registerNewVechicle([FromBody] JObject dataFromDashboard)
        {
            if (dataFromDashboard == null)
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            bool hasVehicleId = dataFromDashboard.ContainsKey("vehiclePlateNumber");
            // Checking if data structure is correct.
            if (!hasVehicleId)
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            string vehiclePlateNumber;
            try
            {
                vehiclePlateNumber = dataFromDashboard.Value<string>("vehiclePlateNumber");
            }
            catch
            {
                // Bad request.
                return new StatusCodeResult(400);
            }
            Vehicle newVehicle = new Vehicle();
            newVehicle.vehiclePlateNumber = vehiclePlateNumber;
            _context.vehicles.Add(newVehicle);
            try
            {
                _context.SaveChanges();
            }
            catch
            {
                // Unknown error.
                return new StatusCodeResult(500);
            }
            return new StatusCodeResult(204);
        }
    }
}
