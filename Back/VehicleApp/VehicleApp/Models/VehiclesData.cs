﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("vehiclesData")]
    public class VehiclesData
    {
        public int id { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public double speed { get; set; }
        public string dateAdded { get; set; }
        public int vehicleId { get; set; }
    }
}
