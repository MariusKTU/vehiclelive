﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    [Table("vehicles")]
    public class Vehicle
    {
        public int id { get; set; }
        public string  vehiclePlateNumber { get; set; }
        public double? latestLatitude { get; set; }
        public double? latestLongitute { get; set; }
        public double? latestSpeed { get; set; }
    }
}
